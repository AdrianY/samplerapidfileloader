/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.fileloader.loader;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.greatus.rapidfileloader.fileloader.customthreadpool.EmployeeRepositoryService;
import com.greatus.rapidfileloader.fileloader.customthreadpool.FileLoaderService;
import com.greatus.rapidfileloader.fileloader.dao.EmployeeDAO;
import com.greatus.rapidfileloader.fileloader.dao.EmployeeDAOImpl;

public class FileLoaderImpl implements FileLoader
{
	private static final Logger LOG = Logger.getLogger(FileLoaderImpl.class.getName());
	private static final int DEFAULT_NO_OF_FILE_LOADERS = 3;
	private static final int DEFAULT_NO_OF_FILE_WRITERS = 1000;
	private static final int DEFAULT_EXPECTED_NO_OF_RECORDS = 50001;
	
	private EmployeeDAO employeeDAO;
	private EmployeeRepositoryService employeeRepository;
	private FileLoaderService fileLoaderService;
	
	public FileLoaderImpl(){
		employeeDAO = new EmployeeDAOImpl();
		employeeRepository = new EmployeeRepositoryService(DEFAULT_NO_OF_FILE_WRITERS, employeeDAO);
		fileLoaderService = new FileLoaderService(DEFAULT_NO_OF_FILE_LOADERS, employeeRepository);
		
		
		Thread checkerThread = new CheckerThread(DEFAULT_EXPECTED_NO_OF_RECORDS, fileLoaderService, employeeRepository);
		checkerThread.setName("CheckerThread");
		checkerThread.start();
	}
	
	@Override
	public void loadFileFrom(File directory)
	{
		LOG.log(Level.INFO, "Loading employee files from {0}",directory.getAbsolutePath());
		// the Employee files contains one employee record per line.
				// for each file found in the path, load each data line into employee object and save the object.
				// call employeeDAO to delegate saving of each employee record.
				// implement it so that it can load the data files in a very efficient and extremely fast manner.
		File [] files = directory.listFiles();
		LOG.log(Level.INFO, "Number of files to be processed: {0}",files.length);
		for(File file: files){
			fileLoaderService.submitTask(file);
		}
	}
	
}
