package com.greatus.rapidfileloader.fileloader.dao;

import java.util.Comparator;

import com.greatus.rapidfileloader.model.Employee;

public class EmployeeCache extends EnhancedCache<Long, Employee>{
	
	private static EmployeeCache instance = new EmployeeCache();
	
	public static EmployeeCache getCurrentCache(){
		return instance;
	}
	
	private EmployeeCache(){super();}
	
	public void add(Employee employee){
		cache.put(employee.getId(), employee);
	}
	
	public boolean exists(Employee employee) {
		return cache.containsKey(employee.getId());
	}

	@Override
	protected Comparator<Long> setupComparator() {
		return new Comparator<Long>(){

			@Override
			public int compare(Long o1, Long o2) {
				return o1.intValue() - o2.intValue();
			}
			
		};
	}

}
