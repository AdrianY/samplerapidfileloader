package com.greatus.rapidfileloader.fileloader.customthreadpool;

import com.greatus.rapidfileloader.fileloader.dao.EmployeeCache;
import com.greatus.rapidfileloader.fileloader.dao.EmployeeDAO;
import com.greatus.rapidfileloader.model.Employee;

public class EmployeeRepositoryService extends
		EnhancedThreadPoolService<Employee> {
	
	private EmployeeDAO employeeDAO;

	public EmployeeRepositoryService(final int numberOfThreads, final EmployeeDAO employeeDAO) {
		super(numberOfThreads);
		this.employeeDAO = employeeDAO;
	}

	@Override
	public void submitTask(final Employee employee) {
		loadTask(new Runnable(){
			public void run(){
				if(!EmployeeCache.getCurrentCache().exists(employee))
					employeeDAO.saveEmployee(employee);
			}
		});
	}

}
