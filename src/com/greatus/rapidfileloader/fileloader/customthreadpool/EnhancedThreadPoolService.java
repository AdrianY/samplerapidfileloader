package com.greatus.rapidfileloader.fileloader.customthreadpool;

import java.util.ArrayList;
import java.util.List;

public abstract class EnhancedThreadPoolService<V>{
	
	private static final int DEFAULT_CAPACITY_PER_THREAD = 1000;

	List<WorkerThread> workerThreads;
	BlockingQueue taskQueue;
	
	EnhancedThreadPoolService(final int numberOfThreads){	
		taskQueue = new BlockingQueue(numberOfThreads * DEFAULT_CAPACITY_PER_THREAD);
		workerThreads = new ArrayList<WorkerThread>();
		for(int i = 0; i < numberOfThreads; i++){
			WorkerThread thread = new WorkerThread(taskQueue);
			thread.setName(this.getClass().getSimpleName()+"-WorkerThread-"+i);
			workerThreads.add(thread);		
		}
		startThreads();
	}
	
	public abstract void submitTask(V object);
	
	protected void loadTask(Runnable task){
		try {
			taskQueue.put(task);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void shutDown(){
		for(WorkerThread thread: workerThreads){
			thread.doStop();
		}
	}
	
	private void startThreads(){
		for(WorkerThread thread: workerThreads){
			thread.start();
		}
	}
	
	private class WorkerThread extends Thread{
		
		private boolean isStopped;
		private BlockingQueue taskQueue;
		
		WorkerThread(final BlockingQueue taskQueue){
			isStopped = false;
			this.taskQueue = taskQueue;
		}
		
		public void run(){
			while(!isStopped){
				try {
					Runnable task = taskQueue.releaseTaskItem();
					task.run();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public synchronized void doStop(){
			if(!isStopped) isStopped = true;
		}
	}
}
