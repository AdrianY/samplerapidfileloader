package com.greatus.rapidfileloader.fileloader.customthreadpool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.greatus.rapidfileloader.model.Employee;

public class FileLoaderService extends EnhancedThreadPoolService<File>{
	
	private static final String SPLIT_BY_COMMA = ",";

	EmployeeRepositoryService employeeRepositoryService;
	
	public FileLoaderService(final int numberOfThreads, final EmployeeRepositoryService employeeRepository) {
		super(numberOfThreads);
		employeeRepositoryService = employeeRepository;
	}

	@Override
	public void submitTask(final File fileToBeRead) {
		loadTask(new Runnable(){
			public void run(){
				try {
					BufferedReader bufferedReader = new BufferedReader(new FileReader(fileToBeRead));
					String line = "";
					
					line = bufferedReader.readLine();
					line = bufferedReader.readLine();
					while(null != line){
						String [] splitText = line.split(SPLIT_BY_COMMA);
						
						persistEmployeeData(Long.parseLong(splitText[0]), splitText[1],splitText[2],splitText[3]);
						line = bufferedReader.readLine();			
					}
					
					bufferedReader.close();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			
			private void persistEmployeeData(
					final Long id,
					final String firstName,
					final String lastName,
					final String address
			) throws InterruptedException{
				Employee employee = new Employee();
				employee.setId(id);
				employee.setAddress(address);
				employee.setFirstName(firstName);
				employee.setLastName(lastName);
				
				employeeRepositoryService.submitTask(employee);

			}
		});
	}

}
