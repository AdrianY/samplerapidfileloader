package com.greatus.rapidfileloader.fileloader.customthreadpool;

import java.util.ArrayList;
import java.util.List;

class BlockingQueue {
	
	private List<Runnable> taskItems;
	
	private int capacity;
	
	public BlockingQueue(int capacity){
		taskItems = new ArrayList<Runnable>();
		this.capacity = capacity;
	}
	
	public synchronized void put(Runnable taskItem) throws InterruptedException{
		while(capacity <= taskItems.size()){
			wait();
		}
		
		if(capacity > taskItems.size())
			notifyAll();
		
		taskItems.add(taskItem);
	}

	public synchronized Runnable releaseTaskItem() throws InterruptedException{
		while(0 == taskItems.size()){
			wait();
		}
		
		if(0 < taskItems.size())
			notifyAll();
		
		return taskItems.remove(0);
	}
}
