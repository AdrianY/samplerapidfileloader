/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.runner;

import com.greatus.rapidfileloader.fileloader.loader.FileLoader;
import com.greatus.rapidfileloader.fileloader.loader.FileLoaderImpl;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author donro
 */
public class FileLoaderMain
{
	private static final Logger LOG = Logger.getLogger(FileLoaderMain.class.getName());
	
	public static void main(String[] args)
	{
		LOG.log(Level.INFO, "Starting file loader process");
		FileLoader fileLoader = new FileLoaderImpl();
		fileLoader.loadFileFrom(new File("./input"));
		LOG.log(Level.INFO, "loader process completed");
	}
}
