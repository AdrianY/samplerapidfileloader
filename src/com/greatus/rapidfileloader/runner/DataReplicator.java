/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greatus.rapidfileloader.runner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.stream.FileImageOutputStream;

/**
 *
 * @author donro
 */
public class DataReplicator
{
	private static final Logger LOG = Logger.getLogger(DataReplicator.class.getName());
	
	public static void main(String[] args)
	{
		File inputFile = new File("./excel/Source.csv");
		int fileCount = 1000;
		for (int i = 0; i < fileCount; i++)
		{
			try
			{
				copyFile(inputFile, new File("./input/Source-" + i + ".csv"));
			}
			catch (IOException ex)
			{
				LOG.log(Level.SEVERE, ex.getMessage(),ex);
			}
		}
	}

	private static void copyFile(File inputFile, File outputFile) throws FileNotFoundException, IOException
	{
		FileImageOutputStream target = null;
		FileInputStream source = null;
		int bufferSize = (int) inputFile.length();
		byte[] buffer = new byte[bufferSize];
		try
		{
			source = new FileInputStream(inputFile);
			source.read(buffer);

			target = new FileImageOutputStream(outputFile);
			target.write(buffer);
		}
		finally
		{
			if (source != null)
			{
				source.close();
			}
			if (target != null)
			{
				target.close();
			}
		}
	}
}
